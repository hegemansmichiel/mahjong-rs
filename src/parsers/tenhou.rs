use crate::tile::{Tile, SuitTile, WindTile, DragonTile};
use std::fmt::Formatter;
use crate::tile::Tile::{Wind, Dragon};
use std::str::FromStr;

#[derive(Debug)]
pub struct TenhouTiles {
    tiles: Vec<Tile>,
}

impl Into<Vec<Tile>> for TenhouTiles {
    fn into(self) -> Vec<Tile> {
        self.tiles
    }
}

impl AsRef<[Tile]> for TenhouTiles {
    fn as_ref(&self) -> &[Tile] {
        &self.tiles
    }
}

impl FromStr for TenhouTiles {
    type Err = TenhouParseTilesError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut res: Vec<Tile> = vec![];
        let mut tiles: Vec<u8> = vec![];

        for (i, c) in s.chars().enumerate() {
            match c {
                '0' => tiles.push(0),
                '1' => tiles.push(1),
                '2' => tiles.push(2),
                '3' => tiles.push(3),
                '4' => tiles.push(4),
                '5' => tiles.push(5),
                '6' => tiles.push(6),
                '7' => tiles.push(7),
                '8' => tiles.push(8),
                '9' => tiles.push(9),
                'm' => {
                    let t = construct_tiles(&tiles, TileType::M)?;
                    res.extend(t);
                    tiles.clear();
                }
                's' => {
                    let t = construct_tiles(&tiles, TileType::S)?;
                    res.extend(t);
                    tiles.clear();
                }
                'p' => {
                    let t = construct_tiles(&tiles, TileType::P)?;
                    res.extend(t);
                    tiles.clear();
                }
                'z' => {
                    let t = construct_tiles(&tiles, TileType::Z)?;
                    res.extend(t);
                    tiles.clear();
                }
                _ => {
                    return Err(TenhouParseTilesError::InvalidCharacter(c, i));
                }
            }
        }

        Ok(TenhouTiles { tiles: res })
    }
}

#[derive(Debug)]
pub enum TenhouParseTilesError {
    InvalidCharacter(char, usize),
    InvalidTileNumber(u8, TileType),
}

impl std::fmt::Display for TenhouParseTilesError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            TenhouParseTilesError::InvalidCharacter(character, location) =>
                write!(f, "Invalid character {} found at location {}", character, location),
            TenhouParseTilesError::InvalidTileNumber(number, t) =>
                write!(f, "Invalid number {} for type: {}", number, t),
        }
    }
}

#[derive(Debug)]
pub enum TileType {
    M,
    P,
    S,
    Z,
}

impl std::fmt::Display for TileType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            TileType::M => write!(f, "{}", "Man"),
            TileType::P => write!(f, "{}", "Pin"),
            TileType::S => write!(f, "{}", "Sou"),
            TileType::Z => write!(f, "{}", "Honor"),
        }
    }
}

fn construct_suit_tiles(numbers: &[u8]) -> Result<Vec<SuitTile>, u8> {
    numbers.iter()
        .map(|n| {
            match n {
                0 => Ok(SuitTile::Five),
                1 => Ok(SuitTile::One),
                2 => Ok(SuitTile::Two),
                3 => Ok(SuitTile::Three),
                4 => Ok(SuitTile::Four),
                5 => Ok(SuitTile::Five),
                6 => Ok(SuitTile::Six),
                7 => Ok(SuitTile::Seven),
                8 => Ok(SuitTile::Eight),
                9 => Ok(SuitTile::Nine),
                i => Err(i.clone()),
            }
        })
        .collect::<Result<Vec<SuitTile>, u8>>()
}

fn construct_honor_tiles(numbers: &[u8]) -> Result<Vec<Tile>, u8> {
    numbers.iter()
        .map(|n| {
            match n {
                1 => Ok(Wind(WindTile::East)),
                2 => Ok(Wind(WindTile::South)),
                3 => Ok(Wind(WindTile::West)),
                4 => Ok(Wind(WindTile::North)),
                5 => Ok(Dragon(DragonTile::White)),
                6 => Ok(Dragon(DragonTile::Green)),
                7 => Ok(Dragon(DragonTile::Red)),
                i => Err(i.clone()),
            }
        })
        .collect()
}

fn construct_tiles(numbers: &[u8], tile_type: TileType) -> Result<Vec<Tile>, TenhouParseTilesError> {
    let t = match tile_type {
        TileType::Z => {
            construct_honor_tiles(numbers)
                .map_err(|err| TenhouParseTilesError::InvalidTileNumber(err, tile_type))?
        }
        TileType::M => {
            construct_suit_tiles(numbers)
                .map_err(|err| TenhouParseTilesError::InvalidTileNumber(err, tile_type))?
                .into_iter()
                .map(|s| Tile::Man(s))
                .collect()
        }
        TileType::P => {
            construct_suit_tiles(numbers)
                .map_err(|err| TenhouParseTilesError::InvalidTileNumber(err, tile_type))?
                .into_iter()
                .map(|s| Tile::Pin(s))
                .collect()
        }
        TileType::S => {
            construct_suit_tiles(numbers)
                .map_err(|err| TenhouParseTilesError::InvalidTileNumber(err, tile_type))?
                .into_iter()
                .map(|s| Tile::Sou(s))
                .collect()
        }
    };

    Ok(t)
}

#[cfg(test)]
mod test {
    use crate::tile::{Tile, SuitTile, WindTile, DragonTile, MAN_1, WIND_SOUTH, MAN_3};
    use crate::parsers::tenhou::{TenhouParseTilesError, TenhouTiles};
    use std::assert_matches::assert_matches;

    macro_rules! single_tile_tests {
        ($($name:ident: $value:expr,) *) => {
            $(
            #[test]
            fn $name() {
                let (input, expected) = $value;

                let tiles: TenhouTiles = input.parse().unwrap();
                assert_eq!(tiles.tiles, vec![expected]);
            }
            )*
        }
    }

    single_tile_tests! {
        man_0_test: ("0m", Tile::Man(SuitTile::Five)),
        man_1_test: ("1m", Tile::Man(SuitTile::One)),
        man_2_test: ("2m", Tile::Man(SuitTile::Two)),
        man_3_test: ("3m", Tile::Man(SuitTile::Three)),
        man_4_test: ("4m", Tile::Man(SuitTile::Four)),
        man_5_test: ("5m", Tile::Man(SuitTile::Five)),
        man_6_test: ("6m", Tile::Man(SuitTile::Six)),
        man_7_test: ("7m", Tile::Man(SuitTile::Seven)),
        man_8_test: ("8m", Tile::Man(SuitTile::Eight)),
        man_9_test: ("9m", Tile::Man(SuitTile::Nine)),
        pin_0_test: ("0p", Tile::Pin(SuitTile::Five)),
        pin_1_test: ("1p", Tile::Pin(SuitTile::One)),
        pin_2_test: ("2p", Tile::Pin(SuitTile::Two)),
        pin_3_test: ("3p", Tile::Pin(SuitTile::Three)),
        pin_4_test: ("4p", Tile::Pin(SuitTile::Four)),
        pin_5_test: ("5p", Tile::Pin(SuitTile::Five)),
        pin_6_test: ("6p", Tile::Pin(SuitTile::Six)),
        pin_7_test: ("7p", Tile::Pin(SuitTile::Seven)),
        pin_8_test: ("8p", Tile::Pin(SuitTile::Eight)),
        pin_9_test: ("9p", Tile::Pin(SuitTile::Nine)),
        sou_0_test: ("0s", Tile::Sou(SuitTile::Five)),
        sou_1_test: ("1s", Tile::Sou(SuitTile::One)),
        sou_2_test: ("2s", Tile::Sou(SuitTile::Two)),
        sou_3_test: ("3s", Tile::Sou(SuitTile::Three)),
        sou_4_test: ("4s", Tile::Sou(SuitTile::Four)),
        sou_5_test: ("5s", Tile::Sou(SuitTile::Five)),
        sou_6_test: ("6s", Tile::Sou(SuitTile::Six)),
        sou_7_test: ("7s", Tile::Sou(SuitTile::Seven)),
        sou_8_test: ("8s", Tile::Sou(SuitTile::Eight)),
        sou_9_test: ("9s", Tile::Sou(SuitTile::Nine)),
        hon_1_test: ("1z", Tile::Wind(WindTile::East)),
        hon_2_test: ("2z", Tile::Wind(WindTile::South)),
        hon_3_test: ("3z", Tile::Wind(WindTile::West)),
        hon_4_test: ("4z", Tile::Wind(WindTile::North)),
        hon_5_test: ("5z", Tile::Dragon(DragonTile::White)),
        hon_6_test: ("6z", Tile::Dragon(DragonTile::Green)),
        hon_7_test: ("7z", Tile::Dragon(DragonTile::Red)),
    }

    #[test]
    fn order_independent_test() {
        let tiles: TenhouTiles = "1m2z3m".parse().unwrap();

        assert_eq!(tiles.tiles, vec![MAN_1, WIND_SOUTH, MAN_3]);
    }

    #[test]
    fn invalid_char_test() {
        let tiles: Result<TenhouTiles, TenhouParseTilesError> = "1q".parse();

        assert_matches!(tiles, Err(TenhouParseTilesError::InvalidCharacter('q', 1)));
    }

    #[test]
    fn no_numbers_test() {
        let tiles: TenhouTiles = "z".parse().unwrap();
        assert_eq!(tiles.tiles, vec![]);
    }

    #[test]
    fn large_honor_number_test() {
        let tiles: Result<TenhouTiles, TenhouParseTilesError> = "9z".parse();

        assert_matches!(tiles, Err(TenhouParseTilesError::InvalidTileNumber(9, _)));
    }
}
