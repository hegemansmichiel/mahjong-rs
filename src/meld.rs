use crate::tile::{Tile, SuitTile};
use std::fmt::Formatter;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Meld {
    Chi(Chi),
    Pon(Pon),
}

impl std::fmt::Display for Meld {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Meld::Chi(c) => write!(f, "{}{}{}", c.tiles[0], c.tiles[1], c.tiles[2]),
            Meld::Pon(p) => write!(f, "{}{}{}", p.tiles[0], p.tiles[1], p.tiles[2]),
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Chi {
    pub tiles: [Tile; 3],
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Pon {
    pub tiles: [Tile; 3],
}

#[derive(Debug, Eq, PartialEq)]
pub struct Pair {
    pub tiles: [Tile; 2],
}

impl std::fmt::Display for Pair {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}{}", self.tiles[0], self.tiles[1])
    }
}

#[derive(Debug)]
pub struct MeldError;

#[derive(Debug)]
pub enum FindError {
    Exhausted,
    NotFound,
}

fn build_index_selector(len: usize, from: usize, skip: &[usize]) -> Vec<usize> {
    (from..len).into_iter()
        .filter(|x| !skip.contains(x))
        .collect()
}

impl Pon {
    pub fn find(tiles: &[Tile], from: usize, skip: &[usize]) -> Result<(Pon, [usize; 3]), FindError> {
        let range = build_index_selector(tiles.len(), from, skip);

        if range.len() < 3 {
            return Err(FindError::Exhausted);
        }

        if let Ok(pon) = pon(tiles[range[0]], tiles[range[1]], tiles[range[2]]) {
            return Ok((pon, [range[0], range[1], range[2]]));
        }

        Err(FindError::NotFound)
    }
}

pub fn pon(a: Tile, b: Tile, c: Tile) -> Result<Pon, MeldError> {
    if a == b && b == c {
        Ok(Pon { tiles: [a, b, c] })
    } else {
        Err(MeldError)
    }
}

fn is_valid_chi(x: SuitTile, y: SuitTile, z: SuitTile) -> bool {
    let mut tiles = vec![x, y, z];
    tiles.sort();

    let mut comps = tiles[0..2].iter().zip(tiles[1..3].iter());
    comps.all(|(&u, &v)| u.is_next(&v))
}

impl Chi {
    pub fn find(tiles: &[Tile], from: usize, skip: &[usize]) -> Result<(Chi, [usize; 3]), FindError> {
        let range = build_index_selector(tiles.len(), from, skip);

        if range.len() < 3 {
            return Err(FindError::Exhausted);
        }

        // We need to jump over values, since we can have situations like 112233
        // i, j will be index 1 and 2, 0 is locked at from.
        for i in 1..(range.len() - 1) {
            for j in 2..range.len() {
                if let Ok(chi) = chi(tiles[from], tiles[range[i]], tiles[range[j]]) {
                    return Ok((chi, [from, range[i], range[j]]));
                }
            }
        }

        Err(FindError::NotFound)
    }
}

pub fn chi(a: Tile, b: Tile, c: Tile) -> Result<Chi, MeldError> {
    match (a, b, c) {
        (Tile::Man(x), Tile::Man(y), Tile::Man(z)) if is_valid_chi(x, y, z) => Ok(Chi { tiles: [a, b, c] }),
        (Tile::Sou(x), Tile::Sou(y), Tile::Sou(z)) if is_valid_chi(x, y, z) => Ok(Chi { tiles: [a, b, c] }),
        (Tile::Pin(x), Tile::Pin(y), Tile::Pin(z)) if is_valid_chi(x, y, z) => Ok(Chi { tiles: [a, b, c] }),
        _ => Err(MeldError),
    }
}

impl Pair {
    pub fn find(tiles: &[Tile], from: usize, skip: &[usize]) -> Result<(Pair, [usize; 2]), FindError> {
        let range = build_index_selector(tiles.len(), from, skip);

        if range.len() < 2 {
            return Err(FindError::Exhausted);
        }

        let mut previous = (None, 0, 0, 0);

        for index in range {
            let current = tiles[index];

            match previous {
                (Some(t), c, _, p) if t == current => previous = (Some(current), c + 1, p, index),
                (Some(t), 2, p, c) => {
                    return Ok((Pair { tiles: [t, t] }, [p, c]));
                },
                _ => previous = (Some(current), 1, 0, index),
            }
        }

        match previous {
            (Some(t), 2, i1, i2) => Ok((Pair { tiles: [t, t] }, [i1, i2])),
            _ => Err(FindError::Exhausted),
        }
    }
}

pub fn pair(a: Tile, b: Tile) -> Result<Pair, MeldError> {
    if a == b {
        Ok(Pair { tiles: [a, b] })
    } else {
        Err(MeldError)
    }
}

#[cfg(test)]
mod test {
    use crate::tile::Tile;
    use std::str::FromStr;
    use crate::meld::{Pon, pon, Chi, chi, Pair, pair, MeldError};
    use std::assert_matches::assert_matches;

    #[test]
    fn pon_test() {
        let pin_1_1 = Tile::from_str("P1").unwrap();
        let pin_1_2 = Tile::from_str("P1").unwrap();
        let pin_1_3 = Tile::from_str("P1").unwrap();

        let pon = pon(pin_1_1, pin_1_2, pin_1_3);

        assert_matches!(pon, Ok(Pon { tiles: _ }));
    }

    #[test]
    fn pon_fail_test() {
        let pin_1_1 = Tile::from_str("P1").unwrap();
        let pin_1_2 = Tile::from_str("P1").unwrap();
        let pin_2_1 = Tile::from_str("P2").unwrap();

        let pon = pon(pin_1_1, pin_1_2, pin_2_1);

        assert_matches!(pon, Err(MeldError));
    }

    #[test]
    fn chi_test() {
        let pin_1 = Tile::from_str("P1").unwrap();
        let pin_2 = Tile::from_str("P2").unwrap();
        let pin_3 = Tile::from_str("P3").unwrap();

        let chi = chi(pin_1, pin_2, pin_3);

        assert_matches!(chi, Ok(Chi { tiles: _ }));
    }

    #[test]
    fn chi_fail_test() {
        let pin_1 = Tile::from_str("P1").unwrap();
        let pin_2 = Tile::from_str("P2").unwrap();
        let pin_4 = Tile::from_str("P4").unwrap();

        let chi = chi(pin_1, pin_2, pin_4);

        assert_matches!(chi, Err(MeldError));
    }

    #[test]
    fn pair_test() {
        let dragon_red_1 = Tile::from_str("DR").unwrap();
        let dragon_red_2 = Tile::from_str("DR").unwrap();

        let pair = pair(dragon_red_1, dragon_red_2);

        assert_matches!(pair, Ok(Pair { tiles: _ }))
    }

    #[test]
    fn pair_fail_test() {
        let dragon_red = Tile::from_str("DR").unwrap();
        let wind_east = Tile::from_str("WE").unwrap();

        let pair = pair(dragon_red, wind_east);

        assert_matches!(pair, Err(MeldError));
    }

    #[test]
    fn find_pair_find_after_pon_test() {
        let tile_1 = Tile::from_str("C1").unwrap();
        let tile_2 = Tile::from_str("C1").unwrap();
        let tile_3 = Tile::from_str("C1").unwrap();
        let tile_4 = Tile::from_str("C2").unwrap();
        let tile_5 = Tile::from_str("C2").unwrap();

        let tiles = [tile_1, tile_2, tile_3, tile_4, tile_5];

        let (pair, skip) = Pair::find(&tiles, 0, &[]).unwrap();

        assert_eq!(pair, Pair { tiles: [tile_4, tile_5] });
        assert_eq!(skip, [3, 4])
    }
}