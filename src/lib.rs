#![feature(assert_matches)]
#![feature(is_sorted)]
#![feature(slice_group_by)]

use crate::tile::{Tile, SuitTile, DragonTile, WindTile};
use crate::hand::Hand;
use crate::meld::Pair;
use std::fmt::Formatter;
use crate::hand::algorithm::search_all;

pub mod tile;
pub mod meld;
pub mod hand;
pub mod parsers;

#[derive(Debug)]
pub struct Kokushi {
    pub duplicate: Tile,
}

impl std::fmt::Display for Kokushi {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "C1C9P1P9B1B9DRDWDGWEWSWWWN {}", self.duplicate)
    }
}

#[derive(Debug)]
pub struct SevenPairs {
    pub a: Pair,
    pub b: Pair,
    pub c: Pair,
    pub d: Pair,
    pub e: Pair,
    pub f: Pair,
    pub g: Pair,
}

impl std::fmt::Display for SevenPairs {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} {} {} {} {} {}", self.a, self.b, self.c, self.d, self.e, self.f, self.g)
    }
}

#[derive(Debug)]
pub enum HandReading {
    Kokushi(Kokushi),
    SevenPairs(SevenPairs),
    Hand(Hand),
}

impl std::fmt::Display for HandReading {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            HandReading::Kokushi(k) => write!(f, "Kokushi: {}", k),
            HandReading::SevenPairs(p) => write!(f, "Chiitoitsu: {}", p),
            HandReading::Hand(h) => write!(f, "Hand: {}", h),
        }
    }
}

fn try_kokushi(tiles: &Vec<Tile>) -> Option<Kokushi> {
    let required = vec![
        Tile::Man(SuitTile::One),
        Tile::Man(SuitTile::Nine),
        Tile::Pin(SuitTile::One),
        Tile::Pin(SuitTile::Nine),
        Tile::Sou(SuitTile::One),
        Tile::Sou(SuitTile::Nine),
        Tile::Dragon(DragonTile::Red),
        Tile::Dragon(DragonTile::White),
        Tile::Dragon(DragonTile::Green),
        Tile::Wind(WindTile::East),
        Tile::Wind(WindTile::South),
        Tile::Wind(WindTile::West),
        Tile::Wind(WindTile::North),
    ];

    let mut req_index = 0;
    let mut duplicate = None;

    for i in 0..14 {
        let tile = tiles[i];
        let required_tile = required[req_index];

        if tile == required_tile {
            req_index += 1;
            continue;
        }

        if let Some(_) = duplicate {
            // We already found a double, so no more need to check for another
            return None;
        }

        if req_index == 0 {
            // We're at the start, so we cannot check previous for a duplicate
            return None;
        }

        if tile == required[req_index - 1] {
            // Perhaps this is a double, so check last required value to see if there's a match.
            duplicate = Some(tile);
            continue;
        }

        // Party over!
        return None;
    }

    // We only ever reach this statement if we've found a duplicate
    Some(Kokushi { duplicate: duplicate.unwrap() })
}

fn try_seven_pairs(tiles: &Vec<Tile>) -> Option<SevenPairs> {
    if tiles.len() != 14 {
        return None;
    }

    let pairs = tiles.chunks(2);

    let mut previous_tile = None;
    let mut tile_pairs = vec![];

    for pair in pairs {
        if pair[0] != pair[1] {
            return None;
        }

        let tile = Some(pair[0]);

        if tile == previous_tile {
            // Cannot have same pairs in seven pairs.
            return None;
        }

        previous_tile = tile;

        tile_pairs.push(Pair { tiles: [pair[0], pair[1]] })
    }

    return Some(SevenPairs {
        a: tile_pairs.pop().unwrap(),
        b: tile_pairs.pop().unwrap(),
        c: tile_pairs.pop().unwrap(),
        d: tile_pairs.pop().unwrap(),
        e: tile_pairs.pop().unwrap(),
        f: tile_pairs.pop().unwrap(),
        g: tile_pairs.pop().unwrap(),
    });
}

pub fn find_hand_readings(tiles: &mut Vec<Tile>) -> Vec<HandReading> {
    if tiles.len() != 14 {
        return vec![];
    }

    tiles.sort();

    if let Some(kokushi) = try_kokushi(tiles) {
        return vec![HandReading::Kokushi(kokushi)];
    }

    if let Some(pairs) = try_seven_pairs(tiles) {
        // Could still also be Ryanpeikou
        let mut readings = search_all(tiles)
            .into_iter()
            .map(|x| HandReading::Hand(x))
            .collect::<Vec<HandReading>>();

        readings.push(HandReading::SevenPairs(pairs));

        return readings;
    }

    search_all(tiles).into_iter().map(|x| HandReading::Hand(x)).collect()
}

pub fn is_mahjong<T>(tiles: T) -> bool where T: Into<Vec<Tile>> {
    let mut tiles = tiles.into();
    let readings = find_hand_readings(&mut tiles);

    return readings.len() != 0;
}

#[cfg(test)]
mod test {
    use crate::tile::Tile;
    use crate::try_seven_pairs;
    use std::assert_matches::assert_matches;

    #[test]
    fn is_seven_pairs_test() {
        let tiles = Tile::hand_from_exact_str("DRDRWEWEC1C1C3C3P1P1B4B4B6B6").unwrap();

        assert_matches!(try_seven_pairs(&tiles), Some(_));
    }
}