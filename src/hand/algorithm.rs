use crate::tile::Tile;
use crate::hand::{Hand, group_tiles, HandPreference};
use std::convert::TryInto;
use std::cmp::Ordering;
use crate::meld::{Meld as CrateMeld, Chi, Pon, Pair};

pub fn search_all(tiles: &Vec<Tile>) -> Vec<Hand> {
    /*
    When mod 3 == 2, it's a likely group candidate to contain the pair. And possible pon/chi.
    When mod 3 == 0, it's a group that potentially has melds (pon or chi) in them.

    Any other results will never result in a complete hand.
     */
    let mut pon_readings = vec![];
    let mut chi_readings = vec![];

    for group in group_tiles(tiles) {
        match group.len() % 3 {
            0 | 2 => (),
            _ => return vec![],
        }

        if let Some(v) = find_melds(group, &[], true, HandPreference::Pon) {
            pon_readings.push(v);
        }

        if let Some(v) = find_melds(group, &[], true, HandPreference::Chi) {
            chi_readings.push(v);
        }
    }

    let pon_reading: Option<Hand> = pon_readings.into_iter().flat_map(|x| x).collect::<Vec<Meld>>().try_into().ok();
    let chi_reading: Option<Hand> = chi_readings.into_iter().flat_map(|x| x).collect::<Vec<Meld>>().try_into().ok();

    match (pon_reading, chi_reading) {
        (Some(p), Some(c)) if p == c => vec![p],
        (Some(p), Some(c)) if p != c => vec![p, c],
        (Some(p), _) => vec![p],
        (_, Some(c)) => vec![c],
        (_, _) => vec![],
    }
}

#[derive(Debug)]
enum Meld {
    Pon(Tile),
    Chi(Tile),
    Pair(Tile),
}

impl TryInto<CrateMeld> for Meld {
    type Error = ();

    fn try_into(self) -> Result<CrateMeld, Self::Error> {
        match self {
            Meld::Pair(_) => Err(()),
            Meld::Chi(t) => {
                let second = t.next_chi_tile().unwrap();
                let third = second.next_chi_tile().unwrap();
                Ok(CrateMeld::Chi(Chi { tiles: [t, second, third] }))
            },
            Meld::Pon(t) => {
                Ok(CrateMeld::Pon(Pon { tiles: [t, t, t] }))
            }
        }
    }
}

impl TryInto<Pair> for Meld {
    type Error = ();

    fn try_into(self) -> Result<Pair, Self::Error> {
        match self {
            Meld::Pair(t) => Ok(Pair { tiles: [t, t] }),
            _ => Err(()),
        }
    }
}

impl TryInto<Hand> for Vec<Meld> {
    type Error = ();

    fn try_into(self) -> Result<Hand, Self::Error> {
        if self.len() != 5 {
            return Err(());
        }

        let mut v = self;

        // Sort so pair meld comes last and we can pop it as first one.
        // Also sort pon and chi so hands are considered equal.
        v.sort_by(|a, b| {
            match (a, b) {
                (Meld::Chi(_), Meld::Chi(_)) => Ordering::Equal,
                (Meld::Pon(_), Meld::Chi(_)) => Ordering::Greater,
                (Meld::Pon(_), Meld::Pon(_)) => Ordering::Equal,
                (Meld::Chi(_), Meld::Pon(_)) => Ordering::Less,
                (Meld::Pair(_), Meld::Pair(_)) => Ordering::Equal,
                (Meld::Pair(_), _) => Ordering::Greater,
                (_, Meld::Pair(_)) => Ordering::Less,
            }
        });

        let pair: Result<Pair, ()> = v.pop().unwrap().try_into();
        let meld1: Result<CrateMeld, ()> = v.pop().unwrap().try_into();
        let meld2: Result<CrateMeld, ()> = v.pop().unwrap().try_into();
        let meld3: Result<CrateMeld, ()> = v.pop().unwrap().try_into();
        let meld4: Result<CrateMeld, ()> = v.pop().unwrap().try_into();

        match (pair, meld1, meld2, meld3, meld4) {
            (Ok(pair), Ok(a), Ok(b), Ok(c), Ok(d)) => Ok(Hand { pair, melds: [a, b, c, d] }),
            _ => Err(()),
        }
    }
}

fn find_melds(group: &[Tile], used_indices: &[usize], can_pair: bool, preference: HandPreference) -> Option<Vec<Meld>> {
    // The goal is to use up every tile to make melds, else it's not useful.
    if group.len() == used_indices.len() {
        return Some(vec![]);
    }

    if let Some(p) = find_pair(group, &used_indices, can_pair) {
        let mut used_indices = used_indices.to_vec();
        used_indices.extend_from_slice(&p);

        if let Some(mut v) = find_melds(group, &used_indices, false, preference) {
            v.push(Meld::Pair(group[p[0]]));
            return Some(v);
        }
    }

    match preference {
        HandPreference::Pon => {
            if let Some(p) = find_pon(group, &used_indices) {
                let mut used_indices = used_indices.to_vec();
                used_indices.extend_from_slice(&p);

                if let Some(mut v) = find_melds(group, &used_indices, can_pair, preference) {
                    v.push(Meld::Pon(group[p[0]]));
                    return Some(v);
                }
            }

            if let Some(p) = find_chi(group, &used_indices) {
                let mut used_indices = used_indices.to_vec();
                used_indices.extend_from_slice(&p);

                if let Some(mut v) = find_melds(group, &used_indices, can_pair, preference) {
                    v.push(Meld::Chi(group[p[0]]));
                    return Some(v);
                }
            }
        }
        HandPreference::Chi => {
            if let Some(p) = find_chi(group, &used_indices) {
                let mut used_indices = used_indices.to_vec();
                used_indices.extend_from_slice(&p);

                if let Some(mut v) = find_melds(group, &used_indices, can_pair, preference) {
                    v.push(Meld::Chi(group[p[0]]));
                    return Some(v);
                }
            }

            if let Some(p) = find_pon(group, &used_indices) {
                let mut used_indices = used_indices.to_vec();
                used_indices.extend_from_slice(&p);

                if let Some(mut v) = find_melds(group, &used_indices, can_pair, preference) {
                    v.push(Meld::Pon(group[p[0]]));
                    return Some(v);
                }
            }
        }
    }

    None
}

fn find_pon(tiles: &[Tile], used_indices: &[usize]) -> Option<[usize; 3]> {
    let range = build_index_selector(tiles.len(), used_indices);

    if range.len() < 3 {
        return None;
    }

    let (idx, idy, idz) = (range[0], range[1], range[2]);
    let (a, b, c) = (tiles[idx], tiles[idy], tiles[idz]);

    if a == b && b == c {
        Some([idx, idy, idz])
    } else {
        None
    }
}

fn is_valid_chi(x: &Tile, y: &Tile, z: &Tile) -> bool {
    match (x.as_numbered(), y.as_numbered(), z.as_numbered()) {
        (Some(x), Some(y), Some(z)) => {
            let tiles = vec![x, y, z];

            let left = tiles[0..2].iter();
            let right = tiles[1..3].iter();

            left.zip(right).all(|(&u, &v)| u.is_next(v))
        }
        _ => false,
    }
}

fn find_chi(tiles: &[Tile], used_indices: &[usize]) -> Option<[usize; 3]> {
    let range = build_index_selector(tiles.len(), used_indices);

    if range.len() < 3 {
        return None;
    }

    for i in 1..(range.len() - 1) {
        for j in (i + 1)..range.len() {
            let (idx, idy, idz) = (range[0], range[i], range[j]);
            let (a, b, c) = (tiles[idx], tiles[idy], tiles[idz]);

            if is_valid_chi(&a, &b, &c) {
                return Some([idx, idy, idz]);
            }
        }
    }

    None
}

fn find_pair(tiles: &[Tile], used_indices: &[usize], can_pair: bool) -> Option<[usize; 2]> {
    if !can_pair {
        return None;
    }

    let range = build_index_selector(tiles.len(), used_indices);

    let mut previous = (None, 0);

    for index in range {
        let current = tiles[index];

        match previous {
            (Some(t), p) if t == current => return Some([p, index]),
            _ => previous = (Some(current), index),
        }
    }

    None
}

fn build_index_selector(len: usize, used_indices: &[usize]) -> Vec<usize> {
    (0..len).into_iter()
        .filter(|x| !used_indices.contains(x))
        .collect()
}

#[cfg(test)]
mod test {
    use crate::parsers::tenhou::TenhouTiles;
    use crate::hand::algorithm::search_all;

    /*
    111 123 456 789 99
    111 22 345 678 999
    11 123 345 678 999
    111 234 456 789 99
    111 234 55 678 999
    11 123 456 678 999
    111 234 567 789 99
    111 234 567 88 999
    11 123 456 789 999
     */

    macro_rules! should_match_tests {
        ($($name:ident: $value:expr,) *) => {
            $(
            #[test]
            fn $name() {
                let tiles: TenhouTiles = $value.parse().unwrap();
                let melds = search_all(&tiles.into());

                assert_eq!(melds.len(), 1);
            }
            )*
        }
    }

    should_match_tests! {
        chuuren_1_test: "11112345678999s",
        chuuren_2_test: "11122345678999s",
        chuuren_3_test: "11123345678999s",
        chuuren_4_test: "11123445678999s",
        chuuren_5_test: "11123455678999s",
        chuuren_6_test: "11123456678999s",
        chuuren_7_test: "11123456778999s",
        chuuren_8_test: "11123456788999s",
        chuuren_9_test: "11123456789999s",
    }

    #[test]
    fn chuuren_2_test_debug() {
        let tiles: TenhouTiles = "11122345678999s".parse().unwrap();
        let melds = search_all(&tiles.into());

        assert_eq!(melds.len(), 1);
    }
}
