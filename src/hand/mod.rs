pub mod algorithm;

use crate::meld::{Meld, Pair};
use crate::tile::{Tile, DragonTile, WindTile};
use std::slice::GroupBy;
use std::fmt::Formatter;

#[derive(Debug, Eq, PartialEq)]
pub struct Hand {
    pub melds: [Meld; 4],
    pub pair: Pair,
}

impl std::fmt::Display for Hand {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} {} {} {}", self.melds[0], self.melds[1], self.melds[2], self.melds[3], self.pair)
    }
}

pub fn group_tiles(tiles: &Vec<Tile>) -> GroupBy<'_, Tile, fn(&Tile, &Tile) -> bool> {
    tiles.group_by(|x, y| {
        match (x, y) {
            (Tile::Man(_), Tile::Man(_)) => true,
            (Tile::Sou(_), Tile::Sou(_)) => true,
            (Tile::Pin(_), Tile::Pin(_)) => true,
            (Tile::Dragon(DragonTile::Red), Tile::Dragon(DragonTile::Red)) => true,
            (Tile::Dragon(DragonTile::White), Tile::Dragon(DragonTile::White)) => true,
            (Tile::Dragon(DragonTile::Green), Tile::Dragon(DragonTile::Green)) => true,
            (Tile::Wind(WindTile::East), Tile::Wind(WindTile::East)) => true,
            (Tile::Wind(WindTile::South), Tile::Wind(WindTile::South)) => true,
            (Tile::Wind(WindTile::West), Tile::Wind(WindTile::West)) => true,
            (Tile::Wind(WindTile::North), Tile::Wind(WindTile::North)) => true,
            _ => false,
        }
    })
}

#[derive(Debug, Copy, Clone)]
pub enum HandPreference {
    Pon,
    Chi
}

#[cfg(test)]
mod test {
    use crate::tile::Tile;
    use std::str::FromStr;
    use crate::hand::group_tiles;

    #[test]
    fn test_group_tiles() {
        let tiles = vec![
            Tile::from_str("DR").unwrap(),
            Tile::from_str("C1").unwrap(),
            Tile::from_str("C2").unwrap(),
        ];

        let mut iter = group_tiles(&tiles);

        assert_eq!(iter.next(), Some(&[
            Tile::from_str("DR").unwrap(),
        ][..]));
        assert_eq!(iter.next(), Some(&[
            Tile::from_str("C1").unwrap(),
            Tile::from_str("C2").unwrap(),
        ][..]));
    }
}
