use std::str::FromStr;
use std::fmt::Formatter;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum Tile {
    Man(SuitTile),
    Pin(SuitTile),
    Sou(SuitTile),
    Dragon(DragonTile),
    Wind(WindTile),
}

#[derive(Debug)]
pub struct ParseTileError;

#[derive(Debug)]
pub enum ParseHandError {
    InvalidTile,
    Uniqueness,
    Size,
}

impl std::fmt::Display for Tile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Tile::Man(n) => write!(f, "{}m", n),
            Tile::Pin(n) => write!(f, "{}p", n),
            Tile::Sou(n) => write!(f, "{}s", n),
            Tile::Dragon(d) => write!(f, "{}z", d),
            Tile::Wind(w) => write!(f, "{}z", w),
        }
    }
}

impl FromStr for Tile {
    type Err = ParseTileError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "WE" => Ok(Tile::Wind(WindTile::East)),
            "WS" => Ok(Tile::Wind(WindTile::South)),
            "WW" => Ok(Tile::Wind(WindTile::West)),
            "WN" => Ok(Tile::Wind(WindTile::North)),
            "DR" => Ok(Tile::Dragon(DragonTile::Red)),
            "DW" => Ok(Tile::Dragon(DragonTile::White)),
            "DG" => Ok(Tile::Dragon(DragonTile::Green)),
            "C1" => Ok(Tile::Man(SuitTile::One)),
            "C2" => Ok(Tile::Man(SuitTile::Two)),
            "C3" => Ok(Tile::Man(SuitTile::Three)),
            "C4" => Ok(Tile::Man(SuitTile::Four)),
            "C5" => Ok(Tile::Man(SuitTile::Five)),
            "C6" => Ok(Tile::Man(SuitTile::Six)),
            "C7" => Ok(Tile::Man(SuitTile::Seven)),
            "C8" => Ok(Tile::Man(SuitTile::Eight)),
            "C9" => Ok(Tile::Man(SuitTile::Nine)),
            "P1" => Ok(Tile::Pin(SuitTile::One)),
            "P2" => Ok(Tile::Pin(SuitTile::Two)),
            "P3" => Ok(Tile::Pin(SuitTile::Three)),
            "P4" => Ok(Tile::Pin(SuitTile::Four)),
            "P5" => Ok(Tile::Pin(SuitTile::Five)),
            "P6" => Ok(Tile::Pin(SuitTile::Six)),
            "P7" => Ok(Tile::Pin(SuitTile::Seven)),
            "P8" => Ok(Tile::Pin(SuitTile::Eight)),
            "P9" => Ok(Tile::Pin(SuitTile::Nine)),
            "B1" => Ok(Tile::Sou(SuitTile::One)),
            "B2" => Ok(Tile::Sou(SuitTile::Two)),
            "B3" => Ok(Tile::Sou(SuitTile::Three)),
            "B4" => Ok(Tile::Sou(SuitTile::Four)),
            "B5" => Ok(Tile::Sou(SuitTile::Five)),
            "B6" => Ok(Tile::Sou(SuitTile::Six)),
            "B7" => Ok(Tile::Sou(SuitTile::Seven)),
            "B8" => Ok(Tile::Sou(SuitTile::Eight)),
            "B9" => Ok(Tile::Sou(SuitTile::Nine)),
            _ => Err(ParseTileError)
        }
    }
}

impl Tile {
    pub fn hand_from_exact_str(s: &str) -> Result<Vec<Tile>, ParseHandError> {
        if s.len() != 28 {
            return Err(ParseHandError::Size);
        }

        let tiles = (0..28).step_by(2)
            .map(|i| Tile::from_str(&s[i..=i + 1]).map_err(|_| ParseHandError::InvalidTile))
            .collect::<Result<Vec<Tile>, ParseHandError>>()?;

        if Tile::check_uniqueness(&tiles) {
            Ok(tiles)
        } else {
            Err(ParseHandError::Uniqueness)
        }
    }

    /// Checks if there are at most 4 of each tile in the list.
    fn check_uniqueness(tiles: &Vec<Tile>) -> bool {
        tiles
            .group_by(|a, b| a == b)
            .all(|g| g.len() <= 4)
    }

    pub fn is_numbered(&self) -> bool {
        match self {
            Tile::Man(_) => true,
            Tile::Pin(_) => true,
            Tile::Sou(_) => true,
            _ => false,
        }
    }

    pub fn is_honor(&self) -> bool {
        match self {
            Tile::Dragon(_) => true,
            Tile::Wind(_) => true,
            _ => false,
        }
    }

    pub fn as_numbered(&self) -> Option<&SuitTile> {
        match self {
            Tile::Pin(s) | Tile::Man(s) | Tile::Sou(s) => Some(s),
            _ => None
        }
    }

    pub fn next_chi_tile(&self) -> Option<Tile> {
        match self {
            Tile::Sou(s) => s.next().map(|s| Tile::Sou(s)),
            Tile::Man(m) => m.next().map(|m| Tile::Man(m)),
            Tile::Pin(p) => p.next().map(|p| Tile::Pin(p)),
            _ => None,
        }
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum WindTile {
    East,
    South,
    West,
    North,
}

impl std::fmt::Display for WindTile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            WindTile::East => "1",
            WindTile::South => "2",
            WindTile::West => "3",
            WindTile::North => "4",
        };

        write!(f, "{}", s)
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum DragonTile {
    Red,
    White,
    Green,
}

impl std::fmt::Display for DragonTile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            DragonTile::Red => "7",
            DragonTile::White => "5",
            DragonTile::Green => "6"
        };

        write!(f, "{}", s)
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
#[repr(i32)]
pub enum SuitTile {
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
    Nine = 9,
}

impl std::fmt::Display for SuitTile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", *self as i32)
    }
}

impl SuitTile {
    pub fn is_next(&self, other: &SuitTile) -> bool {
        ((*self as i32) + 1) == (*other as i32)
    }
    pub fn next(&self) -> Option<SuitTile> {
        match self {
            SuitTile::One => Some(SuitTile::Two),
            SuitTile::Two => Some(SuitTile::Three),
            SuitTile::Three => Some(SuitTile::Four),
            SuitTile::Four => Some(SuitTile::Five),
            SuitTile::Five => Some(SuitTile::Six),
            SuitTile::Six => Some(SuitTile::Seven),
            SuitTile::Seven => Some(SuitTile::Eight),
            SuitTile::Eight => Some(SuitTile::Nine),
            _ => None,
        }
    }
}

#[cfg(test)]
mod test {
    use crate::tile::{Tile, DragonTile, ParseTileError, SuitTile, ParseHandError};
    use std::str::FromStr;
    use std::assert_matches::assert_matches;

    #[test]
    fn parse_test() {
        let res = Tile::from_str("DW");

        assert_matches!(res, Ok(Tile::Dragon(DragonTile::White)));
    }

    #[test]
    fn parse_fail_test() {
        let res = Tile::from_str("AA");

        assert_matches!(res, Err(ParseTileError))
    }

    #[test]
    fn parse_too_many_snowflakes_test() {
        let res = Tile::hand_from_exact_str("C1C1C1C1C1C2C2C2DRDRDRWEWEWE");

        assert_matches!(res, Err(ParseHandError::Uniqueness));
    }

    #[test]
    fn parse_just_enough_snowflakes_test() {
        let res = Tile::hand_from_exact_str("C1C1C1C1C2C2C2C2DRDRDRWEWEWE");

        assert_matches!(res, Ok(_));
    }

    #[test]
    fn same_tile_sort_test() {
        let pin_1 = Tile::from_str("P1").unwrap();
        let pin_2 = Tile::from_str("P2").unwrap();
        let pin_3 = Tile::from_str("P3").unwrap();

        let mut pins = vec![pin_3, pin_1, pin_2];
        pins.sort();

        assert_eq!(pins, vec![pin_1, pin_2, pin_3]);
    }

    #[test]
    fn tile_sort_test() {
        let pin_1 = Tile::from_str("P1").unwrap();
        let character_5 = Tile::from_str("C5").unwrap();
        let dragon_red = Tile::from_str("DR").unwrap();
        let wind_south = Tile::from_str("WS").unwrap();

        let mut tiles = vec![wind_south, pin_1, character_5, dragon_red];
        tiles.sort();

        assert_eq!(tiles, vec![character_5, pin_1, dragon_red, wind_south])
    }

    #[test]
    fn is_next_test() {
        let one = SuitTile::One;
        let two = SuitTile::Two;

        assert_eq!(true, one.is_next(&two));
    }
}

pub const MAN_1: Tile = Tile::Man(SuitTile::One);
pub const MAN_2: Tile = Tile::Man(SuitTile::Two);
pub const MAN_3: Tile = Tile::Man(SuitTile::Three);
pub const MAN_4: Tile = Tile::Man(SuitTile::Four);
pub const MAN_5: Tile = Tile::Man(SuitTile::Five);
pub const MAN_6: Tile = Tile::Man(SuitTile::Six);
pub const MAN_7: Tile = Tile::Man(SuitTile::Seven);
pub const MAN_8: Tile = Tile::Man(SuitTile::Eight);
pub const MAN_9: Tile = Tile::Man(SuitTile::Nine);
pub const SOU_1: Tile = Tile::Sou(SuitTile::One);
pub const SOU_2: Tile = Tile::Sou(SuitTile::Two);
pub const SOU_3: Tile = Tile::Sou(SuitTile::Three);
pub const SOU_4: Tile = Tile::Sou(SuitTile::Four);
pub const SOU_5: Tile = Tile::Sou(SuitTile::Five);
pub const SOU_6: Tile = Tile::Sou(SuitTile::Six);
pub const SOU_7: Tile = Tile::Sou(SuitTile::Seven);
pub const SOU_8: Tile = Tile::Sou(SuitTile::Eight);
pub const SOU_9: Tile = Tile::Sou(SuitTile::Nine);
pub const PIN_1: Tile = Tile::Pin(SuitTile::One);
pub const PIN_2: Tile = Tile::Pin(SuitTile::Two);
pub const PIN_3: Tile = Tile::Pin(SuitTile::Three);
pub const PIN_4: Tile = Tile::Pin(SuitTile::Four);
pub const PIN_5: Tile = Tile::Pin(SuitTile::Five);
pub const PIN_6: Tile = Tile::Pin(SuitTile::Six);
pub const PIN_7: Tile = Tile::Pin(SuitTile::Seven);
pub const PIN_8: Tile = Tile::Pin(SuitTile::Eight);
pub const PIN_9: Tile = Tile::Pin(SuitTile::Nine);
pub const WIND_EAST: Tile = Tile::Wind(WindTile::West);
pub const WIND_SOUTH: Tile = Tile::Wind(WindTile::South);
pub const WIND_WEST: Tile = Tile::Wind(WindTile::West);
pub const WIND_NORTH: Tile = Tile::Wind(WindTile::North);
pub const DRAGON_WHITE: Tile = Tile::Dragon(DragonTile::White);
pub const DRAGON_GREEN: Tile = Tile::Dragon(DragonTile::Green);
pub const DRAGON_RED: Tile = Tile::Dragon(DragonTile::Red);
