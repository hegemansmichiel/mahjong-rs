use mahjong::is_mahjong;
use std::process::exit;
use mahjong::parsers::tenhou::TenhouTiles;

use clap::{AppSettings, Clap};
use mahjong::tile::Tile;
use std::str::FromStr;
use std::io;
use std::io::BufRead;

#[derive(Clap)]
#[clap(version = "1.0", author = "Michiel Hegemans")]
#[clap(setting = AppSettings::ColoredHelp)]
struct Opts {
    #[clap(subcommand)]
    subcmd: SubCommand,
}

#[derive(Clap)]
enum SubCommand {
    Mahjong(Mahjong),
    Tenpai(Tenpai),
}

#[derive(Clap)]
struct Mahjong {}

#[derive(Clap)]
struct Tenpai {}

fn mahjong_main(_: Mahjong) {
    let tiles: TenhouTiles = ""
        .parse()
        .expect("Not a valid hand");

    let exit_code = if is_mahjong(tiles) { 0 } else { 1 };

    exit(exit_code);
}

fn is_tenpai(tiles: &[Tile], tile: Tile) -> bool {
    let mut work_tiles = vec![tile];
    work_tiles.extend_from_slice(tiles);

    is_mahjong(work_tiles)
}

fn tenpai_main(_: Tenpai) {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();

    while let Some(line) = lines.next() {
        let line = line.unwrap();
        tenpai_eval(line);
    }
}

fn tenpai_eval(tiles: String) {
    let tiles: TenhouTiles = tiles
        .parse()
        .expect("Not a valid hand");

    let suits = ["C", "P", "B"];
    let suit_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    let tiles: Vec<Tile> = tiles.into();
    let mut winning_tiles = vec![];

    for suit in suits {
        for suit_number in suit_numbers {
            let tile = Tile::from_str(&format!("{}{}", suit, suit_number)).unwrap();

            if is_tenpai(&tiles, tile) {
                winning_tiles.push(tile);
            }
        }
    }

    let honors = ["WE", "WS", "WW", "WN", "DR", "DG", "DW"];

    for honor in honors {
        let tile = Tile::from_str(honor).unwrap();

        if is_tenpai(&tiles, tile) {
            winning_tiles.push(tile);
        }
    }

    println!("{}", winning_tiles.iter().map(|x| format!("{}", x)).collect::<Vec<String>>().join(","));
}

fn main() {
    let opts: Opts = Opts::parse();

    match opts.subcmd {
        SubCommand::Mahjong(m) => mahjong_main(m),
        SubCommand::Tenpai(t) => tenpai_main(t),
    }
}
