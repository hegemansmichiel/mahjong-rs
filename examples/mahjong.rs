use mahjong::tile::Tile;
use mahjong::find_hand_readings;

fn execute(tiles: &str) {
    let mut tiles = Tile::hand_from_exact_str(tiles).unwrap();

    let readings = find_hand_readings(&mut tiles);
    let len = readings.len();

    for ref reading in readings {
        println!("{}", reading);
    }

    println!("Is Mahjong: {0}", len > 0);
}

fn simple() {
    let tiles = "DRDRC1C1C2C2C3C3WEWEWEP9P9P9";
    println!("Simple: {}", &tiles);
    execute(&tiles);
}

fn multiple_readings() {
    let tiles = "DRDRC1C2C3C1C2C3P1P2P3P1P2P3";
    println!("Multiple readings: {}", &tiles);
    execute(&tiles);
}

fn thirteen_orphans() {
    let tiles = "C1P1B1C9P9B9DRDGDWWSWEWWWNC9";
    println!("Thirteen orphans: {}", &tiles);
    execute(&tiles);
}

fn pair_test() {
    let tiles = "C1C1C1C2C2B1B2B3DRDRDRWWWWWW";
    println!("Pair Test: {}", &tiles);
    execute(&tiles);
}

fn main() {
    simple();
    multiple_readings();
    thirteen_orphans();
    pair_test();
}
